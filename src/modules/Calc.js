var addition = require('Addition')();

/**
 * @module Calc
 */

function Calc() {
  return {
    add: addition.add,
    subtract: function subtract(num1, num2) {
      return num1 - num2;
    }
  };
}

// export our module
module.exports = Calc;