/**
 * @module Addition
 */

function Addition() {
  var _sum = 0;

  function _toArray(alo) {
    return Array.prototype.slice.call(alo);
  }

  // function _flatten(array) {
  //   return array.reduce(function (previousVal, currentVal) {
  //     return previousVal.concat(currentVal);
  //   });
  // }

  function _isANumber(val) {
    return typeof val === 'number';
  }

  function _isArray(obj) {
    return typeof obj === 'object' && !!obj.length;
  }

  return {
    /**
     * Adds an arbitrary number of values.
     * @param {*} Reads from the `arguments` object, filtering out non-numbers.
     * @return {Addition} The Addition module.
     */
    add: function add() {
      // TODO: why does extracting the reduce callback cause problems?
      // TODO: fix issues with concatting
      var numbersToAdd = _toArray(arguments).reduce(function (a, b) {
        return a.concat(b);
      }).filter(_isANumber);

      numbersToAdd.forEach(function (num) {
        _sum += num;
      });

      return this;
    },

    /**
     * Returns the sum of an addition operation.
     * @param  {Object} [opts] - an optional options object.
     * @param  {Boolean} [options.clear] - clears the sum before returning.
     * @return {Number} the sum of the addition
     */
    sum: function sum(opts) {
      var localSum;

      if (opts && opts.clear) {
        localSum = _sum;
        _sum = 0;

        return localSum;
      }

      return _sum;
    }
  };
}

module.exports = Addition;