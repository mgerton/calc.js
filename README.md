# Calc.js

A fun toy project I started to show someone the basics of unit testing. Now using it as a playground of sorts to experiment with building a modular library as well as experiment with newer JS features in ES5 and ES6.

## General Design/Approach

In experimenting with a modular approach (and since this started as a Node-centric project), some of the features of this library are:

- Major calculator functions are extracted into separate, exportable modules
- A chainable API for readability
- Eschew "class"-like approach and prefer factory objects and mixin patterns (prototypal inheritance, yo!)
- Zero dependence on 3rd-party libraries at runtime

As the project evolves, I also hope to have:

- Browser and Node support
- Extensive unit test coverage
- Automated build tasks
- Published to NPM and possibly Bower

Since this is more of a fun project and not really intended for use in actual apps, some of these things may vary and it's very likely that radical API changes could happen. I will try to adhere to semver but be warned that things could always change!

## Contributing

Clone the repo, run `npm install`, and run tests with `npm tests` (requires Jasmine to be installed globally: `npm install -g jasmine`)
