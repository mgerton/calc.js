describe('Addition', function () {
  var Addition = require('../../src/modules/Addition');
  var addition;

  beforeEach(function () {
    addition = Addition();
  });

  it('ignores non-numbers', function () {
    var funkySum = addition
      .add(1, 'apples', 4, {})
      .sum();

    expect(funkySum).toEqual(5);
  });

  it('adds two positive integers', function () {
    var sum = addition
      .add(1, 3)
      .sum();

    expect(sum).toEqual(4);
  });

  it('adds positive and negative integers', function () {
    var sum = addition
      .add(1, -3)
      .sum();

      expect(sum).toEqual(-2);
  });

  it('adds integers and decimals', function () {
    var sum = addition
      .add(8, -7, 4.0, 5.525)
      .sum();

    expect(sum).toEqual(10.525);
  });

  it('adds an array of numbers', function () {
    var sum = addition
      .add([1, 4, 5])
      .sum({ clear: true });

    expect(sum).toEqual(10);

    var anotherSum = addition
      .add([1, 4], 8, [10], 3.0, 5.75)
      .sum();

    expect(anotherSum).toEqual(31.75);
  });

  // it('adds an arbitrary amount of numbers', function () {
  //   var
  //   var sum
  // });
});
