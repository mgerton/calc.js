xdescribe('Calc', function() {
  var Calc = require('../../src/modules/Calc');
  var Addition;
  var myCalc;

  beforeEach(function() {
    Addition = jasmine.createSpy('Addition').andReturn({
      add: function () {
        return 5;
      }
    });

    myCalc = Calc(Addition, Subtraction);
  });

  it('has addition method(s)', function () {
    expect(calc.hasOwnProperty('add')).toBe(true);
  });

  // it('should add two integers', function () {
  //   var sum = myCalc.add(2, 3);

  //   expect(myCalc.add).toBeTruthy();
  //   expect(sum).toEqual(5);
  // });

  // it('should subtract two integers', function () {
  //   var difference = myCalc.subtract(8, 5);
  //   expect(difference).toEqual(3);
  // });

  // it('should add an array of integers', function () {
  //   var sumArray = myCalc.addArray([1, 1, 2, 3, 5, 8]);
  //   expect(sumArray).toBe(20);
  // });
});
